resource "aws_vpc" "devops_leonan_vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  tags = {
    Name = "devops_leonan_vpc"
  }
}

resource "aws_subnet" "devops_leonan_public_subnet" {
  vpc_id     = aws_vpc.devops_leonan_vpc.id
  cidr_block = "10.0.1.0/24"
  tags = {
    Name = "devops_leonan_public_subnet"
  }
}

resource "aws_subnet" "devops_leonan_private_subnet" {
  vpc_id     = aws_vpc.devops_leonan_vpc.id
  cidr_block = "10.0.2.0/24"
  tags = {
    Name = "devops_leonan_private_subnet"
  }
}

resource "aws_instance" "devops_leonan_ec2" {
  ami                         = "ami-089a545a9ed9893b6"
  instance_type               = "t2.micro"
  subnet_id                   = aws_subnet.devops_leonan_public_subnet.id
  key_name                    = "desafio-devops-leonan" # Key SSH
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.devops_leonan_sg.id]
  tags = {
    Name = "devops_leonan_ec2"
  }
}

resource "aws_security_group" "devops_leonan_sg" {
  name        = "devops_leonan_sg"
  description = "Permissao SSH, HTTP e HTTPS na instancia EC2"
  vpc_id      = aws_vpc.devops_leonan_vpc.id

  ingress {
    description = "Enable SSH to EC2"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/16"]
  }

  ingress {
    description = "Enable HTTP to EC2"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Enable HTTPS to EC2"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "enable_ssh_http_https"
  }
}