# Desafio 01: Infrastructure-as-code - Terraform

## Requisitos

- Instalação Terraform;
- VScode ou qualquer editor de texto;


## Deploy


1. **Etapa:** Iremos iniciar o Projeto Terraform e suas dependencias com o comando _**terraform init**_.

_**OBS:**_ Dentro do nosso Repositório de Arquivos, será criado alguns arquivos do Provider do Terraform.
![alt text](terraform/imagens/tf_init.png)

2. **Etapa:** Iremos executar o comando _**terraform plan**_ para nos mostrar um Preview e status dos nossos códigos terraform, para ver se estão ok.

3. **Etapa:** Após isso podemos executar o comando _**terraform apply**_ que irá provisionar nossa Infra na AWS.

Teremos o retorno abaixo do Sucesso.
![alt text](terraform/imagens/tf_apply.png)

Poderemos Visualizar no Console AWS.
![alt text](terraform/imagens/resources-aws.png)

4. **Etapa:** Por final podemos destruir a Infraestrutura com o comando _**terraform destroy**_.

Teremos o seguinte retorno abaixo.

![alt text](terraform/imagens/tf_destroy.png)
