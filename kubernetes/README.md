# Desafio 02: Kubernetes

## Deploy de uma App em um Cluster Minikube

Nesse passo a passo irei detalhar o _**Deploy**_ de uma App em um Cluster Minikube.

## Requisitos

- Instalação Minikube;
- VScode ou qualquer editor de texto;

## Deploy


1. **Etapa:** Build da Image Docker:
Iremos Utilizar a Image Criada a partir do repositório abaixo:

[DockerHub Image Desafio DevOps Leonan](https://hub.docker.com/repository/docker/leonanviana/desafio-devops-leonan)



2. **Etapa:**
Neste repositório existe o script _**deploy.sh**_ onde em uma única execução se cria o Cluster com os Manifestos do K8s prontos.
- _**OBS:**_ Basta verificar as variaveis do Script e ajustar conforme necessidade.


3. **Etapa:** Após a execução do Deploy pelo Script, podemos verificar que os Pods do Cluster estão rodando.
![alt text](kubernetes/imagens/All-Pods.png)


4. **Etapa:** Acessando Aplicação no Browser pelo Host configurado no Ingress.
![alt text](kubernetes/imagens/URL-Ingress.png)


- _**OBS:**_ É necessário adicionar o Host informado no arquivo HOSTS do S.O., no Linux é no Diretório /etc/hosts.
![alt text](kubernetes/imagens/Host.png)


- _**OBS:**_ Existe também como acessarmos pelo Service com o comando abaixo:
![alt text](kubernetes/imagens/URl-Service.png)


## Extras
- Realizado Divisão de recursos por _namespaces_, Criado Manifesto _namespace.yaml_.
- Realizado Criação de uma _ENV_ no _Dockerfile_ para alterar na App a exibição de outro nome ao invés de **"Olá, candidato!"**

