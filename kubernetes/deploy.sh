#!/bin/bash

# Variables
# Alterar Conforme SEU Diretório de Arquivos
PATH_MANIFESTS="/home/leonanviana/Labs/Repos/desafio-devops-leonan/kubernetes/manifests-k8s"
PATH_DEPLOY="$PATH_MANIFESTS/deployment.yaml"
PATH_INGRESS="$PATH_MANIFESTS/ingress.yaml"
PATH_NS="$PATH_MANIFESTS/namespace.yaml"
PATH_SVC="$PATH_MANIFESTS/services.yaml"

# Deploy em um Cluster Minikube
minikube start && \
sleep 5 && \
minikube addons enable ingress && \
sleep 5 && \
kubectl apply -f $PATH_NS && \
sleep 5 && \
kubectl apply -f $PATH_DEPLOY && \
sleep 5 && \
kubectl apply -f $PATH_SVC && \
sleep 5 && \
kubectl apply -f $PATH_INGRESS






